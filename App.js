/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import SplashScreen from './src/containers/onboards';
import MainScreen from './src/containers/MainScreen';
import Daily from './src/containers/MainScreen/Daily';
import AuthLoadingScreen from './src/components/Common/AuthLoadingScreen';
import CustomTabView from './src/components/CustomTabView';
import NewReminder from './src/containers/NewReminder';
import {activeTintColor, inactiveTintColor} from './src/common/Defines';

import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import {Provider} from 'react-redux';
import store from './src/redux/store';

const AuthStack = createStackNavigator(
  {
    SignIn: SplashScreen,
  },
  {headerMode: 'none'},
);

const TopTabNavigation = createMaterialTopTabNavigator(
  {
    MonthlyScreen: {
      screen: MainScreen,
    },
    DailyScreen: {
      screen: Daily,
    },
  },
  {
    headerMode: 'none',
    tabBarComponent: props => <CustomTabView {...props} />,
    tabBarOptions: {
      activeTintColor,
      inactiveTintColor,
    },
  },
);

const MainStackNavigator = createStackNavigator(
  {
    Main: TopTabNavigation,
    AddNew: NewReminder,
  },
  {
    headerMode: 'none',
    mode: 'modal',
    initialRouteName: 'Main',
    transparentCard: true,
  },
);

const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: MainStackNavigator,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
      headerMode: 'none',
    },
  ),
);

function InjectMain(Com) {
  class InjMan extends Component {
    constructor(props) {
      super(props);
      this.state = {
        color: 'white',
      };
    }

    setColor = value => {
      this.setState({color: value});
    };

    render() {
      const {screenProps, ...other} = this.props;
      const {color} = this.state;
      return (
        <Provider store={store}>
          <View style={{flexGrow: 1, backgroundColor: color}}>
            <SafeAreaView
              forceInset={{
                bottom: 'never',
              }}
              style={styles.container}>
              <Com
                {...other}
                screenProps={{
                  ...screenProps,
                  setColor: this.setColor,
                }}
              />
            </SafeAreaView>
          </View>
        </Provider>
      );
    }
  }
  return InjMan;
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
});

export default InjectMain(AppNavigator);
