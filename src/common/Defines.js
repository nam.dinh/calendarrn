import {Dimensions, Platform, PixelRatio} from 'react-native';
// import RNDocPicker from 'react-native-document-picker';
// import I18n from '../i18n/I18n';

export const baseWith = 320;
export const baseHeight = 568;
export const screen = Dimensions.get('screen');
export const primaryColor = '#FFFFFF';
export const secondColor = '#2d80b3';
export const bgInputColor = '#f2f2f4';
export const inactiveTintColor = '#605F60';
export const bgLoadingColor = '#F6F7F9';
export const activeTintColor = '#5A95FF';
export const hsb2028351 = '#165a81';
export const hsb1948475 = '#1e9ac0';
export const hsb0085 = '#d8d8d8';
export const textColor = '#333333';
export const placeholderColor = '#8f8e94';
export const pixelSize = 1 / PixelRatio.getPixelSizeForLayoutSize(1);
export const apiGoogleMapKey = 'AIzaSyDMEUElcYrB833T-wkv84spSvLRlx3tY0Q';
export const latitudeDelta = 0.01986;
export const longitudeDelta = (latitudeDelta * screen.width) / screen.height;

export const statusBarHeight = Platform.select({ios: 20, android: 24});

export const textStyle = {
  fontFamily: 'SF UI Display',
  fontSize: Scale(12),
  fontWeight: Platform.select({ios: '300', android: '100'}),
  color: textColor,
};

export function Scale(size = 12) {
  const scaleWith = screen.width / baseWith;
  const scaleHeight = screen.height / baseHeight;
  const scale = Math.min(scaleWith, scaleHeight);
  return Math.ceil(scale * (size + Platform.select({ios: 1, android: 0})));
}

export const navBarHeight = Scale(Platform.select({ios: 35, android: 25}));
export const tabBarHeight = Scale(36);

// export async function pickFile() {
//   try {
//     const res = await RNDocPicker.pick({type: [RNDocPicker.types.allFiles]});
//     return {ok: true, res};
//   } catch (error) {
//     return {ok: false, error};
//   }
// }

export function removeUtf8(str = '') {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  return str.toLowerCase();
}

export function formatCurrency(num = 0) {
  return num.toLocaleString('vi', {currency: 'VND'});
}

// export {I18n};
