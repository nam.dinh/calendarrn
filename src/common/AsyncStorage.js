import AsyncStorage from '@react-native-community/async-storage';

export var saveLogInState = async () => {
  try {
    await AsyncStorage.setItem('did_login', JSON.stringify(true));
  } catch (e) {
    console.log('Cannot store value to async storage');
  }
};

export var getLogInState = async () => {
  try {
    const value = await AsyncStorage.getItem('did_login');
    return !!value;
  } catch (e) {
    console.log('Cannot read from async storage');
  }
};

export var reminders = async () => {
  try {
    const jsonValue = await AsyncStorage.getItem('reminders');
    let data = JSON.parse(jsonValue || '[]');
    return data;
  } catch (e) {
    console.log('Cannot read from async storage');
  }
};

export var saveReminders = async array => {
  try {
    await AsyncStorage.setItem('reminders', JSON.stringify(array));
  } catch (e) {
    console.log('Cannot store value to async storage');
  }
};
