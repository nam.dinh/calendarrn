import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {activeTintColor, screen} from '../../common/Defines';

const widthHeight = 60;

export default class FloatingAddButton extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.props.onAdd}
          style={styles.TouchableOpacityStyle}>
          <Icon name="plus" size={36} color="white" />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: widthHeight,
    height: widthHeight,
    marginLeft: screen.width - widthHeight - 36,
    marginTop: screen.height - widthHeight - 136,
    borderRadius: widthHeight / 2,
    position: 'absolute',
    backgroundColor: activeTintColor,
  },
  TouchableOpacityStyle: {
    width: widthHeight,
    height: widthHeight,
    alignItems: 'center',
    justifyContent: 'center',
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: widthHeight,
    height: widthHeight,
  },
});
