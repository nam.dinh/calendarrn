import React from 'react';
import {ActivityIndicator, StatusBar, View} from 'react-native';
import {getLogInState} from '../../common/AsyncStorage';

export default class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    getLogInState().then(booleanValue => {
      console.log('boolean value:', booleanValue);
      this.props.navigation.navigate(booleanValue ? 'App' : 'Auth');
    });
  }

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
