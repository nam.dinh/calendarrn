import React, {Component} from 'react';
import {View} from 'react-native';
import {LoginButton} from 'react-native-fbsdk';

export default class FBLoginButton extends Component {
  render() {
    return (
      <View>
        <LoginButton
          publishPermissions={['email']}
          onLoginFinished={(error, result) => {
            if (error) {
              console.log('Login failed with error: ' + error.message);
            } else if (result.isCancelled) {
              console.log('Login was cancelled');
            } else {
              console.log('Login success');
              this.props.logInSuccess();
            }
          }}
          onLogoutFinished={() => console.log('User logged out')}
        />
      </View>
    );
  }
}
