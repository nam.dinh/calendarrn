import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {BottomTabBarProps} from 'react-navigation';
import {
  Scale,
  inactiveTintColor,
  tabBarHeight,
  screen,
} from '../../common/Defines';
import {get} from 'lodash';

const TabMenu = {
  DailyScreen: {
    title: 'Daily',
  },
  MonthlyScreen: {
    title: 'Monthly',
  },
};

const paddingLeft = (screen.width - Scale(200)) / 2;

class CustomTabView extends Component<BottomTabBarProps> {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
    };
    this.onTabPress = this.onTabPress.bind(this);
    this.renderTabBottom = this.renderTabBottom.bind(this);
  }

  onTabPress(routeName, index) {
    const {navigation} = this.props;
    if (index !== navigation.state.index) {
      navigation.navigate(routeName);
    }
  }

  renderTabBottom(item, index) {
    let {navigation, activeTintColor} = this.props;
    let backgroundColor =
      index === navigation.state.index ? activeTintColor : 'white';
    let borderWidth = index === navigation.state.index ? 1 : 0;
    let height =
      index === navigation.state.index
        ? tabBarHeight
        : tabBarHeight - Scale(18);
    let textColor =
      index === navigation.state.index ? 'white' : inactiveTintColor;
    return (
      <View
        style={[
          styles.container,
          // eslint-disable-next-line react-native/no-inline-styles
          {
            flex: 1,
            width: Scale(100),
            backgroundColor: backgroundColor,
            borderWidth: borderWidth,
            height: height,
          },
        ]}>
        <TouchableOpacity
          style={styles.touchable}
          onPress={() => {
            this.onTabPress(item.routeName, index);
          }}
          key={item.key}>
          <Text style={[styles.labelItem, {color: textColor}]}>
            {get(TabMenu[item.key], 'title')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const routers = get(this.props, 'navigation.state.routes', []);
    return (
      <View style={styles.viewContainer}>
        <View
          style={[styles.container, {width: Scale(200), height: tabBarHeight}]}>
          {routers.map(this.renderTabBottom)}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    paddingLeft: paddingLeft,
    backgroundColor: 'white',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#C8CDDA',
    borderWidth: 0.5,
    borderRadius: Scale(18),
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelItem: {
    fontSize: Scale(12),
    textAlign: 'center',
  },
  touchable: {
    height: tabBarHeight,
    justifyContent: 'center',
    flex: 1,
  },
});

export default CustomTabView;
