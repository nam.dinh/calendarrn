import React from 'react';
import {FlatList, View, StyleSheet} from 'react-native';

import PageIndicator from './PageIndicator';

export default class ViewPager extends React.Component {
  state = {
    page: 0,
  };

  onScrollEnded(event) {
    let native = event.nativeEvent;
    let xOffset = native.contentOffset.x;
    let xSize = native.contentSize.width;
    let pageWidth = xSize / this.props.data.length;
    let page = Math.floor(xOffset / pageWidth);
    this.setState({
      page: page,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.list}
          data={this.props.data}
          renderItem={({item}) => this.props.renderItem(item)}
          keyExtractor={(_, index) => `list-item-${index}`}
          initialNumToRender={8}
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          onMomentumScrollEnd={event => this.onScrollEnded(event)}
        />
        <PageIndicator
          style={styles.indicator}
          pages={this.props.data.length}
          selectedIndex={this.state.page}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
  },
  list: {
    height: 100,
  },
  indicator: {
    height: 50,
    width: 200,
  },
});
