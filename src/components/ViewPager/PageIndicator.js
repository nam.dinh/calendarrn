import React from 'react';
import {View, StyleSheet} from 'react-native';

export default class PageIndicator extends React.Component {
  render() {
    var views = [];
    for (let i = 0; i < this.props.pages; i++) {
      let isSelected = this.props.selectedIndex === i;
      let color = isSelected
        ? {backgroundColor: 'red'}
        : {backgroundColor: 'white'};
      views.push(<View style={[styles.indicator, color]} key={i} />);
    }
    return <View style={styles.container}>{views}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  indicator: {
    width: 12,
    height: 12,
    borderRadius: 6,
    borderColor: 'grey',
    borderWidth: 1,
    margin: 8,
  },
});
