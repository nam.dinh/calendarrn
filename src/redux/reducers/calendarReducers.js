import {UPDATE_MARKED_TYPES} from '../actions/type';

const calendar = (state = {}, action) => {
  switch (action.type) {
    case UPDATE_MARKED_TYPES:
      const map1 = function({item}) {
        return {[item.date]: {color: item.color}};
      };
      const reduce1 = function(a, b) {
        return {...a, ...b};
      };
      return action.array.map(map1).reduce(reduce1, {});
    default:
      return state;
  }
};

export default calendar;
