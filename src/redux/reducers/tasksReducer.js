import {ADD, REMOVE, RETRIEVE} from '../actions/type';
import {saveReminders} from '../../common/AsyncStorage';

const todos = (state = [], action) => {
  switch (action.type) {
    case ADD:
      let array = [
        ...state,
        {
          id: action.id,
          item: action.item,
          completed: false,
        },
      ];
      saveReminders(array);
      return array;
    case REMOVE:
      let arr = state.filter(item => item.id !== action.id);
      saveReminders(arr);
      return arr;
    case RETRIEVE:
      state = action.array;
      return state;
    default:
      console.log('???', action);
      return state;
  }
};

export default todos;
