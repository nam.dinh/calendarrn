import {combineReducers} from 'redux';
import todos from './tasksReducer';

export default combineReducers({
  todos,
});
