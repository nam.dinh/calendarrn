export const ADD = 'add';
export const REMOVE = 'remove';
export const RETRIEVE = 'retrieve';
export const UPDATE_MARKED_TYPES = 'updateMarkedTypes';
