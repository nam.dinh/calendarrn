import {ADD, REMOVE, RETRIEVE, UPDATE_MARKED_TYPES} from './type';

// export function addNewItem(item) {
//   return {payload: item, type: ADD};
// }

let nextId = 1;

export const addNewItem = item => ({
  type: ADD,
  id: nextId++,
  item,
});

export const removeItem = item => ({
  item,
  type: REMOVE,
});

export const retrieveData = array => ({
  type: RETRIEVE,
  array,
});

export const getMarkedCalendar = array => ({
  type: UPDATE_MARKED_TYPES,
  array,
});
