import React from 'react';
import {View, StyleSheet, FlatList} from 'react-native';

import FloatingAddButton from '../../components/FloatingAddButton';
import {CalendarList} from 'react-native-calendars';
import ReminderRow from './ReminderRow';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {retrieveData, getMarkedCalendar} from '../../redux/actions';
import {reminders} from '../../common/AsyncStorage';

class MainScreen extends React.Component {
  state = {
    selected: {},
    array: [],
  };

  componentDidMount() {
    reminders().then(array => {
      this.props.actions.retrieveData(array);
      this.setState({
        selected: this.getMarkedDates(),
        array: this.props.todos,
      });
    });
  }

  render() {
    let dates = this.getMarkedDates();
    return (
      <View style={styles.container}>
        <CalendarList
          horizontal={true}
          pagingEnabled={true}
          markedDates={this.state.selected}
          onDayPress={day => {
            // Select dates
            let selections = {};
            if (dates[day.dateString]) {
              let update = {...dates[day.dateString], selected: true};
              dates[day.dateString] = update;
              selections = dates;
            } else {
              selections = {...dates, [day.dateString]: {selected: true}};
            }

            //Filter array
            console.log('todos:', this.state.array);
            let filteredList = this.props.todos.filter(
              item => item.item.date === day.dateString,
            );
            this.setState({
              selected: selections,
              array: filteredList,
            });
          }}
        />
        <FlatList
          style={styles.list}
          data={this.state.array}
          extraData={this.props.todos.length}
          renderItem={({item}) => this.renderRow(item)}
          keyExtractor={(_, index) => `list-item-${index}`}
        />
        <FloatingAddButton
          onAdd={() => this.props.navigation.navigate('AddNew')}
        />
      </View>
    );
  }

  getMarkedDates() {
    const map1 = function({item}) {
      return {[item.date]: {dotColor: item.color, marked: true}};
    };
    const reduce1 = function(a, b) {
      return {...a, ...b};
    };
    return this.props.todos.map(map1).reduce(reduce1, {});
  }

  renderRow(item) {
    return (
      <ReminderRow
        key={item.id}
        item={item}
        showDetail={() => {
          // this.props.navigation.push('Details', { item: story });
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
  },
  tabView: {
    height: 60,
  },
  list: {
    flex: 1,
  },
});

const mapStateToProps = state => ({
  todos: state.todos,
  calendar: state.calendar,
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({retrieveData, getMarkedCalendar}, dispatch),
  };
}

// eslint-disable-next-line prettier/prettier
export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
