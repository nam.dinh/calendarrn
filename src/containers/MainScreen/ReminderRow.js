import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {screen} from '../../common/Defines';

// import TimeAgo from 'javascript-time-ago';
// import en from 'javascript-time-ago/locale/en';

export default class ReminderRow extends React.Component {
  render() {
    let item = this.props.item.item;
    return (
      <View style={styles.container}>
        <View style={styles.titleContentView}>
          <Icon style={styles.icon} name="check-circle-outline" size={20} />
          <Text style={styles.title}>{item.topic}</Text>
          <Text style={styles.title}>{item.date}</Text>
        </View>
        <View style={styles.separator} />
        <View style={styles.titleContentView}>
          <Text style={styles.detail}>View details</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 32,
    marginRight: 32,
    marginTop: 16,
    marginBottom: 16,
    height: 120,
    width: screen.width - 64,
    backgroundColor: 'white',
  },
  titleContentView: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  icon: {
    margin: 16,
  },
  title: {
    marginRight: 32,
    marginTop: 16,
    marginBottom: 16,
  },
  separator: {
    height: 1,
    backgroundColor: 'gray',
    marginLeft: 8,
    width: screen.width - 80,
  },
  detail: {
    margin: 16,
  },
});
