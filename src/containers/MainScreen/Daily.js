import React from 'react';
import {View, StyleSheet, FlatList, Alert} from 'react-native';

import FloatingAddButton from '../../components/FloatingAddButton';
import {CalendarList} from 'react-native-calendars';
import ReminderRow from './ReminderRow';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {removeItem} from '../../redux/actions';
import {reminders} from '../../common/AsyncStorage';

import {SwipeListView} from 'react-native-swipe-list-view';
import mom from 'moment';

class Daily extends React.Component {
  state = {
    array: this.props.todos.filter(
      item => item.item.date === mom().format('YYYY-MM-DD'),
    ),
  };

  onPressDelete = (item, index) => {
    Alert.alert(
      'Delete this event',
      '',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            this.props.actions.removeItem(item);
            this.setState({
              array: this.props.todos.filter(
                item => item.item.date === mom().format('YYYY-MM-DD'),
              ),
            });
          },
        },
      ],
      {cancelable: false},
    );
  };

  renderRow(item) {
    return (
      <ReminderRow
        key={item.id}
        item={item}
        showDetail={() => {
          // this.props.navigation.push('Details', { item: story });
        }}
      />
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.list}
          data={this.state.array}
          extraData={this.props.todos.length}
          renderItem={({item}) => this.renderRow(item)}
          keyExtractor={(_, index) => `list-item-${index}`}
        />
        <FloatingAddButton
          onAdd={() => this.props.navigation.navigate('AddNew')}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({removeItem}, dispatch),
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F7FAFB',
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15,
  },
  logo: {
    height: 40,
    width: 40,
  },
});

// eslint-disable-next-line prettier/prettier
export default connect(mapStateToProps, mapDispatchToProps)(Daily);
