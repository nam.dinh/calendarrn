import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {activeTintColor, screen} from '../../common/Defines';

const widthHeight = 32;

export default class CancelButton extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.props.onAdd}
          style={styles.TouchableOpacityStyle}>
          <Icon name="close" size={20} color="white" />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: widthHeight,
    height: widthHeight,
    marginLeft: 16,
    marginTop: 16,
    borderRadius: widthHeight / 2,
    backgroundColor: 'red',
  },
  TouchableOpacityStyle: {
    width: widthHeight,
    height: widthHeight,
    alignItems: 'center',
    justifyContent: 'center',
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: widthHeight,
    height: widthHeight,
  },
});
