import React from 'react';
import {StyleSheet} from 'react-native';
import {screen} from '../../common/Defines';

import DatePicker from 'react-native-datepicker';
import Moment from 'moment';

const width = screen.width - 84;

export default class CustomDatePicker extends React.Component {
  state = {
    currentDate: '',
  };

  componentDidMount() {
    Moment.locale('en');
  }

  render() {
    return (
      <DatePicker
        style={styles.container}
        date={this.state.currentDate}
        mode="datetime"
        placeholder="select date"
        format="MMMM Do YYYY, h:mm:ss a"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        onDateChange={date => {
          let time = Moment(date, 'MMMM Do YYYY, h:mm:ss a').format(
            'YYYY-MM-DD',
          );
          this.setState({currentDate: time});
          this.props.dateDidChange(time);
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: width,
    height: 50,
    marginLeft: 32,
    marginRight: 32,
    marginBottom: 16,
  },
});
