import {StyleSheet} from 'react-native';
import {Scale, activeTintColor} from '../../common/Defines';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0202029c',
  },
  content: {
    margin: 16,
    backgroundColor: 'white',
    borderRadius: 16,
  },
  headerTitle: {
    fontSize: Scale(20),
    margin: 32,
    textAlign: 'center',
  },
  title: {
    fontSize: Scale(12),
    marginLeft: 32,
    marginRight: 32,
    marginBottom: 16,
  },
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: 'lightgrey',
    marginBottom: 32,
  },
  color: {
    marginBottom: 32,
  },
  marginLeftRight: {
    marginLeft: 32,
    marginRight: 32,
  },
  confirm: {
    backgroundColor: activeTintColor,
    borderBottomRightRadius: 16,
    borderBottomLeftRadius: 16,
    height: 44,
  },
  opacity: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addText: {
    fontSize: 14,
    color: 'white',
  },
  picker: {
    borderWidth: 0.5,
    borderColor: 'lightgray',
    borderRadius: 4,
  },
  underline: {
    textDecorationLine: 'underline',
  },
});
