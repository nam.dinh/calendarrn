import React from 'react';
import {View, Text, TextInput, Picker} from 'react-native';
import {TouchableOpacity, FlatList} from 'react-native-gesture-handler';
import CancelButton from './CancelButton';
import {styles} from './styles';
import CustomDatePicker from './CustomDatePicker';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addNewItem} from '../../redux/actions';

class NewReminder extends React.Component {
  constructor(props) {
    super(props);
    this.title = '';
    this.desc = '';
    this.date = '';
    this.notificationOptions = [
      {
        label: 'On time',
        value: 'ontime',
        notiBefore: 0,
      },
      {
        label: 'Before 10 mins',
        value: '10min',
        notiBefore: 10,
      },
      {
        label: 'Before 30 mins',
        value: '30min',
        notiBefore: 30,
      },
    ];
  }

  state = {
    showTimePicker: false,
    timeBefore: {
      label: 'On time',
      value: 'ontime',
      notiBefore: 0,
    },
    colors: [
      {color: 'tomato', selected: false},
      {color: 'limegreen', selected: false},
      {color: 'deeppink', selected: false},
      {color: 'dodgerblue', selected: false},
    ],
  };

  componentDidMount() {
    this.props.screenProps.setColor('#0202029c');
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <CancelButton
            onAdd={() => {
              this.props.navigation.goBack();
              this.props.screenProps.setColor('white');
            }}
          />
          <Text style={styles.headerTitle}>Create New Tasks</Text>
          <Text style={[styles.title, styles.marginLeftRight]}>Topic</Text>
          <TextInput
            style={[styles.textInput, styles.marginLeftRight]}
            placeholder="Write Topic"
            returnKeyType="done"
            onSubmitEditing={e => {
              this.title = e.nativeEvent.text.trim();
            }}
          />
          <Text style={[styles.title, styles.marginLeftRight]}>
            Description
          </Text>
          <TextInput
            style={[styles.textInput, styles.marginLeftRight]}
            placeholder="Write Description"
            returnKeyType="done"
            onSubmitEditing={e => {
              this.desc = e.nativeEvent.text.trim();
            }}
          />
          <CustomDatePicker
            dateDidChange={date => {
              this.date = date;
            }}
          />
          <TouchableOpacity
            onPress={() =>
              this.setState({
                showTimePicker: !this.state.showTimePicker,
              })
            }>
            <Text
              style={[styles.title, styles.underline, styles.marginLeftRight]}>
              Notification: {this.state.timeBefore.label}
            </Text>
          </TouchableOpacity>
          {this.state.showTimePicker && (
            <Picker
              style={[styles.picker, styles.marginLeftRight]}
              selectedValue={this.state.timeBefore.value}
              onValueChange={time => {
                this.setState({
                  timeBefore: time,
                  showTimePicker: false,
                });
              }}>
              <Picker.Item
                label={this.notificationOptions[0].label}
                value={this.notificationOptions[0]}
              />
              <Picker.Item
                label={this.notificationOptions[1].label}
                value={this.notificationOptions[1]}
              />
              <Picker.Item
                label={this.notificationOptions[2].label}
                value={this.notificationOptions[2]}
              />
            </Picker>
          )}
          <Text style={[styles.title, styles.marginLeftRight]}>
            Choose color
          </Text>
          <FlatList
            style={[styles.color, styles.marginLeftRight]}
            horizontal={true}
            data={this.state.colors}
            renderItem={item => this.renderRow(item.item)}
            keyExtractor={(_, index) => `list-item-${index}`}
          />
          <View style={styles.confirm}>
            <TouchableOpacity
              style={styles.opacity}
              onPress={() => {
                this.saveRecord();
                this.props.navigation.goBack();
              }}>
              <Text style={styles.addText}>ADD</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  saveRecord() {
    if (this.title.length === 0) {
      // eslint-disable-next-line no-alert
      alert('Please enter topic name');
      return null;
    }

    if (this.desc.length === 0) {
      // eslint-disable-next-line no-alert
      alert('Please enter description');
      return null;
    }

    let newRecord = {
      topic: this.title,
      description: this.desc,
      date: this.date,
      notification: this.state.timeBefore,
      color: this.state.colors.find(e => e.selected).color,
    };
    this.props.actions.addNewItem(newRecord);
  }

  renderRow(item) {
    let widthHeight = item.selected ? 50 : 40;
    return (
      <TouchableOpacity onPress={() => this.updateSelectedItem(item)}>
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            flexGrow: 1,
            backgroundColor: item.color,
            height: widthHeight,
            width: widthHeight,
            borderRadius: widthHeight / 2,
            margin: 16,
          }}
        />
      </TouchableOpacity>
    );
  }

  updateSelectedItem(item) {
    let temp = this.state.colors;
    temp.forEach(element => (element.selected = element.color === item.color));
    this.setState({
      colors: temp,
    });
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({addNewItem}, dispatch),
  };
}

// eslint-disable-next-line prettier/prettier
export default connect(null, mapDispatchToProps)(NewReminder);
