import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

import ViewPager from '../../components/ViewPager/ViewPager';
import {swipeList} from '../../common/HardcodeData';
import {screen} from '../../common/Defines';

import FBLoginButton from '../../components/Facebook/FBLoginButton';
import TwitterButton from '../../components/Twitter/TwitterButton';
import {saveLogInState} from '../../common/AsyncStorage';

export default class Onboards extends React.Component {
  logInSuccess = () => {
    saveLogInState();
    this.props.navigation.navigate('App');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text>Hi there! Welcome to</Text>
          <Text style={styles.largeText}>TDD</Text>
        </View>
        <ViewPager
          style={styles.swipableContainer}
          data={swipeList}
          renderItem={item => (
            <View style={styles.rowContainer}>
              <Text style={styles.rowText}>{item.text}</Text>
            </View>
          )}
        />
        <View style={styles.loginContainer}>
          <FBLoginButton logInSuccess={this.logInSuccess} />
          <TwitterButton logInSuccess={this.logInSuccess} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  textContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  largeText: {
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 16,
  },
  swipableContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowContainer: {
    width: screen.width,
  },
  rowText: {
    margin: 32,
    textAlign: 'center',
  },
});
